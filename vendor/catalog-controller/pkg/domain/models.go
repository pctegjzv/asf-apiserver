package domain

import (
	"catalog-controller/pkg/apis/catalogcontroller/v1alpha1"
	"catalog-controller/pkg/tools"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/bitly/go-simplejson"
	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)


const (
	// SuccessSynced is used as part of the Event 'reason' when a Release is synced
	SuccessSynced = "Synced"
	// MessageResourceSynced is the message used for an Event fired when a Foo
	// is synced successfully
	MessageResourceSynced       = "Release synced successfully"
	SuccessCreatedRelease       = "Created"
	PartlySuccessCreatedRelease = "PartlyCreated"
	FailedCreatedRelease        = "Failed"
	FailedCreatedResource       = "FailedCreatedResource"
	SuccessCreatedResource      = "SuccessCreatedResource"
)

const (
	FileNameChart          string = "Chart.yaml"
	FileNameChartMetadata  string = "Chart.meta.yaml"
	FileNameValues         string = "values.yaml"
	FileNameValuesMetadata string = "values.meta.yaml"
	FileNameReadme         string = "README.md"
	FileNameTemplates      string = "templates"
)

type YamlFile struct {
	Content  string
	FilePath string
	FileName string
}

func NewYamlFile(yamlFilePath string) (*YamlFile, error) {
	content, _ := tools.ReadFileContent(yamlFilePath)
	yamlFile := YamlFile{
		FilePath: yamlFilePath,
		FileName: filepath.Base(yamlFilePath),
		Content:  content,
	}
	return &yamlFile, nil
}

func (y *YamlFile) toInterface() interface{} {
	return nil
}

type ChartDir struct {
	ChartMetadata  *YamlFile
	README         *YamlFile
	Chart          *YamlFile
	Values         *YamlFile
	ValuesMetadata *YamlFile
	Templates      []*YamlFile
}

func NewChartDir(chartPath string) (*ChartDir, error) {
	var chartDir = ChartDir{}
	subList, err := ioutil.ReadDir(chartPath)
	if err != nil {
		return nil, err
	}
	if len(subList) == 0 {
		return nil, errors.New(chartPath + " is empty")
	}

	for i, fileInfo := range subList {
		fmt.Printf("%d: Reading %s \n", i, fileInfo.Name())
		if fileInfo.IsDir() {
			if fileInfo.Name() == FileNameTemplates {
				templatesPath := filepath.Join(chartPath, FileNameTemplates)
				templates, err := tools.Children(templatesPath, false)
				if err != nil {
					return nil, err
				}
				for j, template := range templates {
					fmt.Printf("-- %d.%d: Reading template %s \n", i, j, template.Name())
					templateFilePath := filepath.Join(templatesPath, template.Name())
					yamlFile, err := NewYamlFile(templateFilePath)
					if err != nil {
						return nil, err
					}
					chartDir.Templates = append(chartDir.Templates, yamlFile)
				}
			}
		} else {
			chartFilePath := filepath.Join(chartPath, fileInfo.Name())
			yamlFile, err := NewYamlFile(chartFilePath)
			if err != nil {
				return nil, err
			}

			switch fileInfo.Name() {
			case FileNameChart:
				chartDir.Chart = yamlFile
			case FileNameChartMetadata:
				chartDir.ChartMetadata = yamlFile
			case FileNameValues:
				chartDir.Values = yamlFile
			case FileNameValuesMetadata:
				chartDir.ValuesMetadata = yamlFile
			case FileNameReadme:
				chartDir.README = yamlFile
			}
		}
	}

	err = chartDir.Validate()
	if err != nil {
		glog.Errorf("Error happens when validate the charts. dir: %s; err: %v", chartPath, err)
		return nil, err
	}

	return &chartDir, nil
}

func (cd *ChartDir) GetYamlFileChartMetadata() (*metav1.ObjectMeta, error) {
	jsonByte, err := tools.ConvertStringToJsonByte(cd.ChartMetadata.Content)
	if err != nil {
		return nil, err
	}
	var obj metav1.ObjectMeta
	err = json.Unmarshal(jsonByte, &obj)
	if err != nil {
		glog.Error("Unmarshal json failed")
		return nil, err
	}
	return &obj, nil
}

func (cd *ChartDir) GetYamlFileChart() (*YamlFileChart, error) {
	jsonByte, err := tools.ConvertStringToJsonByte(cd.Chart.Content)
	if err != nil {
		return nil, err
	}
	var obj YamlFileChart
	err = json.Unmarshal(jsonByte, &obj)
	if err != nil {
		glog.Error("Unmarshal json failed")
		return nil, err
	}
	return &obj, nil
}

func (cd *ChartDir) GetYamlFileValues() (*YamlFileValues, error) {
	jsonByte, err := tools.ConvertStringToJsonByte(cd.Values.Content)
	if err != nil {
		return nil, err
	}
	var obj YamlFileValues
	err = json.Unmarshal(jsonByte, &obj)
	if err != nil {
		glog.Error("Unmarshal json failed")
		return nil, err
	}
	return &obj, nil
}

func (cd *ChartDir) GetYamlFileValuesMetadata() (*YamlFileValuesMetadata, error) {
	var (
		obj YamlFileValuesMetadata
		err error
	)

	if cd.ValuesMetadata != nil {
		jsonByte, err := tools.ConvertStringToJsonByte(cd.ValuesMetadata.Content)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(jsonByte, &obj)
		if err != nil {
			glog.Error("Unmarshal json failed")
			return nil, err
		}
	}
	return &obj, err
}

func (cd *ChartDir) Validate() error {
	if cd.Chart == nil || cd.Chart.FileName == "" {
		return errors.New("Chart.yaml is missed")
	}
	if cd.ChartMetadata == nil || cd.ChartMetadata.FileName == "" {
		return errors.New("Chart.meta.yaml is missed")
	}
	if cd.Values == nil || cd.Values.FileName == "" {
		return errors.New("values.yaml is missed")
	}
	if cd.ValuesMetadata == nil || cd.ValuesMetadata.FileName == "" {
		return errors.New("values.metadata.yaml is missed")
	}
	if cd.Templates == nil || len(cd.Templates) == 0 {
		return errors.New("templates dir is empty")
	}
	return nil
}

func (cd *ChartDir) ConvertToChart() (*v1alpha1.Chart, error) {
	objChart, err := cd.GetYamlFileChart()
	if err != nil {
		fmt.Println("Error when convert Chart.yaml.", err)
		return nil, err
	}

	objChartMetadata, err := cd.GetYamlFileChartMetadata()
	if err != nil {
		fmt.Println("Error when convert Chart.meta.yaml.", err)
		return nil, err
	}

	objValues, err := cd.GetYamlFileValues()
	if err != nil {
		fmt.Println("Error when convert values.yaml.", err)
		return nil, err
	}

	// objValuesMetadata may be nil
	objValuesMetadata, err := cd.GetYamlFileValuesMetadata()
	if err != nil {
		fmt.Println("Error when convert values.meta.yaml.", err)
		return nil, err
	}

	chart := v1alpha1.Chart{}
	chart.Namespace = "default"
	chart.Kind = "Chart"
	chart.Name = objChart.Name
	chart.ObjectMeta.SetCreationTimestamp(v1.Now())
	chart.SetLabels(objChartMetadata.Labels)
	chart.SetAnnotations(objChartMetadata.Annotations)

	chart.Spec.ChartRepository = v1alpha1.ChartSpecChartRepository{
		Name:        "local-repo",
		LocalImport: true,
	}
	if cd.README != nil {
		chart.Spec.Readme = cd.README.Content
	}
	chart.Spec.Values = []v1alpha1.ChartSpecValue{}

	if objValuesMetadata != nil {
		chart.Spec.Values = objValuesMetadata.Arguments
		for _, value := range chart.Spec.Values {
			for i, item := range value.Items {
				data := objValues.GetData(item.Key)
				fmt.Printf("Insert key: %s; value: %v \n", item.Key, data)

				if defaultValue, err := data.String(); err == nil {
					value.Items[i].Schema.Type = v1alpha1.SchemaString
					value.Items[i].Default = defaultValue
				} else if defaultValue, err := data.Bool(); err == nil {
					value.Items[i].Schema.Type = v1alpha1.SchemaBoolean
					value.Items[i].Default = strconv.FormatBool(defaultValue)
				} else if defaultValue, err := data.Int64(); err == nil {
					value.Items[i].Schema.Type = v1alpha1.SchemaInteger
					value.Items[i].Default = strconv.FormatInt(defaultValue, 10)
				} else if defaultValue, err := data.Map(); err == nil {
					value.Items[i].Schema.Type = v1alpha1.SchemaMap
					d, _ := json.Marshal(defaultValue)
					value.Items[i].Default = string(d)
				} else if defaultValue, err := data.Array(); err == nil {
					value.Items[i].Schema.Type = v1alpha1.SchemaArray
					d, _ := json.Marshal(defaultValue)
					value.Items[i].Default = string(d)
				} else {
					value.Items[i].Schema.Type = v1alpha1.SchemaString
					value.Items[i].Default = ""
				}
			}
		}
	}

	// add files
	for _, template := range cd.Templates {
		fmt.Printf("Insert template %s \n", template.FileName)
		chart.Spec.Files = append(chart.Spec.Files, v1alpha1.ChartSpecFile{
			Path:    filepath.Join(FileNameTemplates, template.FileName),
			Content: template.Content,
		})
	}

	chart.Spec.Files = append(chart.Spec.Files, v1alpha1.ChartSpecFile{
		Path:    cd.Chart.FileName,
		Content: cd.Chart.Content,
	})
	chart.Spec.Files = append(chart.Spec.Files, v1alpha1.ChartSpecFile{
		Path:    cd.Values.FileName,
		Content: cd.Values.Content,
	})
	if cd.ChartMetadata != nil {
		chart.Spec.Files = append(chart.Spec.Files, v1alpha1.ChartSpecFile{
			Path:    cd.ChartMetadata.FileName,
			Content: cd.ChartMetadata.Content,
		})
	}
	if cd.ValuesMetadata != nil {
		chart.Spec.Files = append(chart.Spec.Files, v1alpha1.ChartSpecFile{
			Path:    cd.ValuesMetadata.FileName,
			Content: cd.ValuesMetadata.Content,
		})
	}
	if cd.README != nil {
		chart.Spec.Files = append(chart.Spec.Files, v1alpha1.ChartSpecFile{
			Path:    cd.README.FileName,
			Content: cd.README.Content,
		})
	}

	return &chart, nil
}

type YamlFileChart struct {
	Name    string `json:"name"`
	Version string `json:"version"`
}

type YamlFileValues struct {
	simplejson.Json
}

func (y *YamlFileValues) GetData(itemKey string) *simplejson.Json {
	branch := strings.Split(itemKey, ".")
	return y.GetPath(branch...)
}

type YamlFileValuesMetadata struct {
	Arguments []v1alpha1.ChartSpecValue `json:"arguments"`
}

func GetChartsFromLocalRepo(repoPath string) ([]*v1alpha1.Chart, error) {
	subDir, err := tools.SubDir(repoPath)
	if err != nil {
		return nil, err
	}

	chartList := []*v1alpha1.Chart{}
	for i, chartPath := range *subDir {
		if strings.Index(chartPath, ".") == 0 {
			fmt.Printf("Skip the dir: %s \n", chartPath)
			continue
		}

		chartPath = filepath.Join(repoPath, chartPath)
		fmt.Printf("----- [%d] Looking for charts in %s ------ \n", i, chartPath)

		chartDir, err := NewChartDir(chartPath)
		if err != nil {
			glog.Errorf("Error happens when reading the charts. dir: %s; err: %v", chartPath, err)
			continue
		}

		chart, err := chartDir.ConvertToChart()
		if err != nil {
			glog.Errorf("Error happens when convert the charts to YAML. dir: %s; err: %v", chartPath, err)
			continue
		}
		chartList = append(chartList, chart)
	}
	return chartList, nil
}
