/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// SchemaInteger is the int schema
const (
	SchemaInteger = "integer"
	SchemaString  = "string"
	SchemaBoolean = "boolean"
	SchemaMap     = "map"
	SchemaArray   = "array"
)

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Foo is a specification for a Foo resource
type Foo struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   FooSpec   `json:"spec"`
	Status FooStatus `json:"status"`
}

// FooSpec is the spec for a Foo resource
type FooSpec struct {
	DeploymentName string `json:"deploymentName"`
	Replicas       *int32 `json:"replicas"`
}

// FooStatus is the status for a Foo resource
type FooStatus struct {
	AvailableReplicas int32 `json:"availableReplicas"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// FooList is a list of Foo resources
type FooList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Foo `json:"items"`
}

// CatalogSchema CatalogSchema
type CatalogSchema struct {
	Type string `json:"type"`
}

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Release is a specification for a Release resource
type Release struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ReleaseSpec   `json:"spec"`
	Status ReleaseStatus `json:"status"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ReleaseList is a list of release resources
type ReleaseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Release `json:"items"`
}

// ReleaseSpec is the spec for a Release resource
type ReleaseSpec struct {
	Chart     ReleaseSpecChart      `json:"chart"`
	Values    []ReleaseSpeValue     `json:"values"`
	Resources []ReleaseSpecResource `json:"resources"`
	RawValues string                `json:"rawValues,omitempty"`
}

// ReleaseStatus is the status for a Release resource
type ReleaseStatus struct {
	Status     string                   `json:"status"`
	Conditions []ReleaseStatusCondition `json:"conditions"`
}

// ReleaseStatusCondition release condition clause
type ReleaseStatusCondition struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	LastAttempt       *metav1.Time `json:"lastAttempt"`
	Reason            string       `json:"reason,omitempty"`
	Message           string       `json:"message,omitempty"`
	Status            string       `json:"status"`
}

// ConvertToUnstructured convert condition to unstructured
func (r *ReleaseStatusCondition) ConvertToUnstructured() unstructured.Unstructured {
	raw := unstructured.Unstructured{}
	raw.SetKind(r.Kind)
	raw.SetAPIVersion(r.APIVersion)
	raw.SetLabels(r.GetLabels())
	raw.SetName(r.GetName())
	raw.SetNamespace(r.GetNamespace())
	return raw
}

// ReleaseSpecChart is the chart info of the release
type ReleaseSpecChart struct {
	Name    string `json:"Name"`
	Version string `json:"version"`
}

// ReleaseSpeValue is the spec value info of the release
type ReleaseSpeValue struct {
	DisplayName I18NString             `json:"displayName"`
	Items       []ReleaseSpecValueItem `json:"items"`
}

// ReleaseSpecResource is the one resource of the release
type ReleaseSpecResource struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	File              ChartSpecFile `json:"file"`
}

// I18NString is the i18N string
type I18NString struct {
	EN   string `json:"en"`
	ZhCN string `json:"zh-CN"`
}

// ReleaseSpecValueItem is the chart info of the release
type ReleaseSpecValueItem struct {
	Key    string        `json:"key"`
	Value  string        `json:"value"`
	Schema CatalogSchema `json:"schema"`
}

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Chart is a specification for a Chart resource
type Chart struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ChartSpec   `json:"spec"`
	Status ChartStatus `json:"status"`
}

// GetVersion get the version of the chart
func (c *Chart) GetVersion() string {
	labels := c.GetLabels()
	if labels == nil || len(labels) == 0 {
		return ""
	}

	if version, ok := labels["alauda.io/product.version"]; ok {
		return version
	}
	return ""
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ChartList is a list of Chart resources
type ChartList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Release `json:"items"`
}

// ChartSpec is the spec for a Chart resource
type ChartSpec struct {
	ChartRepository ChartSpecChartRepository `json:"ChartRepository"`
	Readme          string                   `json:"readme"`
	Values          []ChartSpecValue         `json:"values"`
	Files           []ChartSpecFile          `json:"files"`
}

// ChartStatus is the status for a Chart resource
type ChartStatus struct {
	AvailableReplicas int32 `json:"availableReplicas"`
}

// ChartSpecChartRepository is the ChartRepository of a Chart resource
type ChartSpecChartRepository struct {
	Name        string `json:"name"`
	LocalImport bool   `json:"localImport"`
}

// ChartSpecValue is the one argument group of a Chart resource
type ChartSpecValue struct {
	DisplayName I18NString           `json:"displayName"`
	Items       []ChartSpecValueItem `json:"items"`
}

// ChartSpecFile is the one file of a Chart resource
type ChartSpecFile struct {
	Path    string `json:"path"`
	Content string `json:"content"`
}

// ChartSpecValueItem is the one argument of a Chart resource
type ChartSpecValueItem struct {
	Key        string                    `json:"key"`
	Display    ChartSpecValueItemDisplay `json:"display"`
	Required   bool                      `json:"required"`
	Schema     CatalogSchema             `json:"schema"`
	Validation ChartValidation           `json:"validation"`
	Default    string
}

// ChartValidation is the validation of a value in a Chart resource
type ChartValidation struct {
	Pattern   string `json:"pattern"`
	MaxLength int    `json:"maxLength"`
}

// ChartSpecValueItemDisplay is one argument display of a chart resource
type ChartSpecValueItemDisplay struct {
	Type        string     `json:"type"`
	Name        I18NString `json:"name"`
	Description I18NString `json:"description"`
}

// BasicResource BasicResource
type BasicResource struct {
	metav1.TypeMeta
	metav1.ObjectMeta `json:"metadata,omitempty" protobuf:"bytes,1,opt,name=metadata"`
}
