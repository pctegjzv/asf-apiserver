package tools

import (
	"encoding/base64"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/ghodss/yaml"
)

// Exist check a file whether exist
func Exist(filePath string) bool {
	_, err := os.Stat(filePath)
	return err == nil || os.IsExist(err)
}

// SubDir get sub dir under the path
func SubDir(dirPath string) (*[]string, error) {
	subList, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	if len(subList) == 0 {
		return nil, errors.New(dirPath + " is empty")
	}
	var subDirs []string
	for i := range subList {
		if subList[i].IsDir() {
			subDirs = append(subDirs, subList[i].Name())
		}
	}
	if len(subDirs) == 0 {
		return nil, errors.New(dirPath + " has no sub dir")
	}
	return &subDirs, nil
}

func Children(dirPath string, isDir bool) ([]os.FileInfo, error) {
	subList, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	if len(subList) == 0 {
		return nil, errors.New(dirPath + " is empty")
	}
	var subDirs []os.FileInfo
	var subFiles []os.FileInfo
	for i, sub := range subList {
		if subList[i].IsDir() {
			subDirs = append(subDirs, sub)
		} else {
			subFiles = append(subFiles, sub)
		}
	}

	if isDir {
		if len(subDirs) == 0 {
			return nil, errors.New(dirPath + " has no sub dir")
		} else {
			return subDirs, nil
		}
	} else {
		if len(subFiles) == 0 {
			return nil, errors.New(dirPath + " has no sub files")
		} else {
			return subFiles, nil
		}
	}
}

func isBinary(contentPoint *[]byte) bool {
	content := *contentPoint
	whiteCount := 0
	for i := range content {
		if content[i] == 9 || content[i] == 10 || content[i] == 13 || (content[i] >= 32 && content[i] <= 255) {
			whiteCount++
		} else if content[i] <= 6 || (content[i] >= 14 && content[i] <= 31) {
			return false
		}
	}
	if whiteCount < 1 {
		return false
	}
	return true
}

// ReadFileContent func
func ReadFileContent(path string) (string, bool) {
	fi, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	fd, err := ioutil.ReadAll(fi)
	if !isBinary(&fd) {
		return base64.StdEncoding.EncodeToString(fd), true
	}
	return string(fd), false
}

func ConvertStringToJsonByte(crdYamlContent string) ([]byte, error) {
	return yaml.YAMLToJSON([]byte(crdYamlContent))
}

// GenFile generate a file
func GenFile(filePath string, fileContent string) error {
	if filePath == "" || fileContent == "" {
		return nil
	}

	paths := strings.Split(filePath, "/")
	// create all dir
	err := os.MkdirAll(strings.Join(paths[:len(paths)-1], "/"), os.ModePerm)
	if err != nil {
		return err
	}
	f, err := os.Create(filePath)
	defer f.Close()
	if err != nil {
		return err
	}
	_, err = io.WriteString(f, fileContent)
	if err != nil {
		return err
	}
	return nil
}
