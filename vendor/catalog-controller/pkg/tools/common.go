package tools

import (
	"encoding/json"
	"fmt"
	"strings"
)

func GetPrettyString(result interface{}) string {
	js, err := json.MarshalIndent(&result, "", "\t")
	if err != nil {
		fmt.Println(err)
	}
	return string(js)
}

func PrintJson(result interface{}) {
	js := GetPrettyString(result)
	fmt.Println(string(js))
}

func IsEmptyString(in string) bool {
	out := trimString(in)
	if out == "" {
		return true
	}
	return false
}

func trimString(in string) (out string) {
	out = strings.TrimSpace(in)
	out = strings.Trim(out, "\n")
	out = strings.Trim(out, "\r")
	return
}
