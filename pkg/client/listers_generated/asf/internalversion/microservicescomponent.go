/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// This file was automatically generated by lister-gen

package internalversion

import (
	asf "alauda.io/asf-apiserver/pkg/apis/asf"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// MicroservicesComponentLister helps list MicroservicesComponents.
type MicroservicesComponentLister interface {
	// List lists all MicroservicesComponents in the indexer.
	List(selector labels.Selector) (ret []*asf.MicroservicesComponent, err error)
	// MicroservicesComponents returns an object that can list and get MicroservicesComponents.
	MicroservicesComponents(namespace string) MicroservicesComponentNamespaceLister
	MicroservicesComponentListerExpansion
}

// microservicesComponentLister implements the MicroservicesComponentLister interface.
type microservicesComponentLister struct {
	indexer cache.Indexer
}

// NewMicroservicesComponentLister returns a new MicroservicesComponentLister.
func NewMicroservicesComponentLister(indexer cache.Indexer) MicroservicesComponentLister {
	return &microservicesComponentLister{indexer: indexer}
}

// List lists all MicroservicesComponents in the indexer.
func (s *microservicesComponentLister) List(selector labels.Selector) (ret []*asf.MicroservicesComponent, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*asf.MicroservicesComponent))
	})
	return ret, err
}

// MicroservicesComponents returns an object that can list and get MicroservicesComponents.
func (s *microservicesComponentLister) MicroservicesComponents(namespace string) MicroservicesComponentNamespaceLister {
	return microservicesComponentNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// MicroservicesComponentNamespaceLister helps list and get MicroservicesComponents.
type MicroservicesComponentNamespaceLister interface {
	// List lists all MicroservicesComponents in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*asf.MicroservicesComponent, err error)
	// Get retrieves the MicroservicesComponent from the indexer for a given namespace and name.
	Get(name string) (*asf.MicroservicesComponent, error)
	MicroservicesComponentNamespaceListerExpansion
}

// microservicesComponentNamespaceLister implements the MicroservicesComponentNamespaceLister
// interface.
type microservicesComponentNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all MicroservicesComponents in the indexer for a given namespace.
func (s microservicesComponentNamespaceLister) List(selector labels.Selector) (ret []*asf.MicroservicesComponent, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*asf.MicroservicesComponent))
	})
	return ret, err
}

// Get retrieves the MicroservicesComponent from the indexer for a given namespace and name.
func (s microservicesComponentNamespaceLister) Get(name string) (*asf.MicroservicesComponent, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(asf.Resource("microservicescomponent"), name)
	}
	return obj.(*asf.MicroservicesComponent), nil
}
