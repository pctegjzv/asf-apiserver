package v1alpha1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

const (
	// ProductName product name
	ProductName = "Alauda ASF"
	// ProjectNamespaceType is a relationship type between both resources
	// currently only "owned" is supported
	MicroservicesEnvironmentKind = "MicroservicesEnvironment"
	MicroservicesComponentKind   = "MicroservicesComponent"
	CatalogKind                  = "Release"
	// MicroservicesEnvironmentStatusCreating creating
	MicroservicesEnvironmentStatusCreating = "Creating"
	// MicroservicesEnvironmentStatusReady can be reached
	MicroservicesEnvironmentStatusReady = "Ready"
	// MicroservicesEnvironmentStatusError service cannot be reached
	MicroservicesEnvironmentStatusError = "Error"

	// ProjectNamespaceTypeOwned ownership type of relationship
	MicroservicesEnvironmentNamespaceTypeOwned = "owned"
	// ProjectNamespaceStatusUnknown status unknown
	MicroservicesEnvironmentNamespaceStatusUnknown = "Unknown"
	// ProjectNamespaceStatusReady status ready
	MicroservicesEnvironmentNamespaceStatusReady = "Ready"
	// ProjectNamespaceStatusError status error
	MicroservicesEnvironmentNamespaceStatusError = "Error"

	MicroservicesEnvironmentComponentsStatusUnknown = "Unknown"

	MicroservicesEnvironmentComponentsStatusBasicReady = "BasicReady"

	MicroservicesEnvironmentComponentsStatusCompletelyReady = "CompletelyReady"

	// MicroservicesEnvironmentBindingStatusTypeMicroservicesEnvironment condition type for MicroservicesEnvironmentBinding
	MicroservicesEnvironmentBindingStatusTypeMicroservicesEnvironment = "MicroservicesEnvironment"
	// MicroservicesEnvironmentBindingStatusTypeSecret condition type for Secret

	// For each condition type we have a list of val id reasons:

	// MicroservicesEnvironmentBindingStatusConditionStatusNotFound when a condition type is NotFound
	MicroservicesEnvironmentBindingStatusConditionStatusNotFound = "NotFound"
	// MicroservicesEnvironmentBindingStatusConditionStatusNotValid when a condition type is not filled
	MicroservicesEnvironmentBindingStatusConditionStatusNotValid = "NotValid"
	// MicroservicesEnvironmentBindingStatusConditionStatusReady when a condition type is Found
	MicroservicesEnvironmentBindingStatusConditionStatusReady = "Ready"
	// Annotations

	// AnnotationsKeyDisplayName displayName key for annotations
	AnnotationsKeyDisplayName = "alauda.io/displayName"
	// AnnotationsKeyDisplayNameEn english displayName key for annotations
	AnnotationsKeyDisplayNameEn = "alauda.io/displayNameEn"
	// AnnotationsKeyProduct product name key for annotations
	AnnotationsKeyMicroservices = "alauda.io/microservices"
	// AnnotationsKeyProductVersion product version key for annotations
	AnnotationsKeyMicroservicesVersion = "alauda.io/microservicesVersion"
	// AnnotationsKeyRoleVersion role version key for annotations
	AnnotationsKeyRoleVersion = "alauda.io/roleVersion"
	// AnnotationsKeyProject project key for annotations
	AnnotationsKeyMicroservicesEnvironment = "alauda.io/microservicesEnvironment"
	AnnotationsKeyMicroservicesComponent   = "alauda.io/microservicesComponent"
	// AnnotationsKeyPipelineLastNumber last number used to generate pipeline store in PipelineConfig
	AnnotationsKeyPipelineLastNumber = "alauda.io/pipeline.last.number"

	// Labels

	// LabelDevopsAlaudaIOKey key used for specific Labels
	LabelDevopsAlaudaIOKey = "devops.alauda.io"
	// LabelDevopsAlaudaIOProjectKey key used for roles that are using in a project
	LabelDevopsAlaudaIOProjectKey = "devops.alauda.io/project"
	// LabelPipelineConfig label key for pipelineConfig
	LabelPipelineConfig = "pipelineConfig"

	// LabelCodeRepoServiceType label key for codeRepoServiceType
	LabelCodeRepoServiceType = "codeRepoServiceType"
	// LabelCodeRepoService label key for codeRepoService
	LabelCodeRepoService = "codeRepoService"
	// LabelCodeRepoBinding label key for codeRepoBinding
	LabelCodeRepoBinding = "codeRepoBinding"
	// LabelCodeRepository label key for codeRepository
	LabelCodeRepository = "codeRepository"

	// TrueString true as string
	TrueString = "true"

	// configmap
	ConfigMapKindName          = "ConfigMap"
	ConfigMapAPIVersion        = "v1"
	SettingsConfigMapNamespace = "alauda-system"
	SettingsConfigMapName      = "devops-config"
	SettingsKeyGithubCreated   = "githubCreated"

	// github
	GithubName = "github"
	GithubHost = "https://api.github.com"
	GithubHtml = "https://github.com"
)

type MicroservicesComponentType int
type MicroservicesComponentStatusType string

const (
	MicroservicesEnvironmentCompnentTypeBasic    int = 0
	MicroservicesEnvironmentCompnentTypeAdvanced int = 1
)
const (
	StatusUnCreate   string = "UnCreate"
	StatusCreated    string = "Created"
	StatusInstalling string = "Installing"
	StatusRunning    string = "Running"
	StatusPending    string = "Pending"
	StatusFailed     string = "Failed"
	StatusStopped    string = "Stopped"
)

type ComponentInfoConstant struct {
	Type             int
	Order            int
	ChartName        string
	SubValueKey2Path map[string][]string
}

// hardcoding chartName for now
// release name is same as component name
var CompInfoMap = map[string]ComponentInfoConstant{
	"zookeeper": ComponentInfoConstant{
		ChartName: "zookeeper",
		Order:     0,
		Type:      MicroservicesEnvironmentCompnentTypeBasic,
	},
	"kafka": ComponentInfoConstant{
		ChartName: "kafka",
		Order:     1,
		Type:      MicroservicesEnvironmentCompnentTypeBasic,
	},
	"eureka": ComponentInfoConstant{
		ChartName: "eureka",
		Order:     2,
		Type:      MicroservicesEnvironmentCompnentTypeBasic,
	},
	"config-server": ComponentInfoConstant{
		ChartName: "config-server",
		Order:     3,
		Type:      MicroservicesEnvironmentCompnentTypeBasic,
		SubValueKey2Path: map[string][]string{
			"profiles": []string{"profiles"},
			"labels":   []string{"labels"},
		},
	},
	"hystrix-dashboard": ComponentInfoConstant{
		ChartName: "hystrix-dashboard",
		Order:     1,
		Type:      MicroservicesEnvironmentCompnentTypeAdvanced,
	},
	"turbine": ComponentInfoConstant{
		ChartName: "turbine",
		Order:     0,
		Type:      MicroservicesEnvironmentCompnentTypeAdvanced,
	},
	"zuul": ComponentInfoConstant{
		ChartName: "zuul",
		Order:     2,
		Type:      MicroservicesEnvironmentCompnentTypeAdvanced,
	},
	"pinpoint-hbase": ComponentInfoConstant{
		ChartName: "pinpoint-hbase",
		Order:     3,
		Type:      MicroservicesEnvironmentCompnentTypeAdvanced,
	},
}

var requestedKeys = []string{AnnotationsKeyMicroservices, AnnotationsKeyMicroservicesVersion}

// IsASFResource returns true for a given ObjectMeta if the resource was created
// by the DevOps Platform
// it will verify some requested keys inside annotations to make sure
func IsASFResource(objMeta metav1.ObjectMeta) bool {
	if len(objMeta.Annotations) > 0 {
		for _, key := range requestedKeys {
			if _, ok := objMeta.Annotations[key]; !ok {
				return false
			}
		}
		return true
	}
	return false
}
