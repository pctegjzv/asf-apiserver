/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package microservicescomponent

import (
	"encoding/json"
	"fmt"
	"log"

	"alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	"alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	"alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset/scheme"
	listers "alauda.io/asf-apiserver/pkg/client/listers_generated/asf/v1alpha1"
	"alauda.io/asf-apiserver/pkg/controller/sharedinformers"

	asfv1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	catav1alpha1 "catalog-controller/pkg/apis/catalogcontroller/v1alpha1"
	catalogclient "catalog-controller/pkg/client/clientset/versioned"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	v1 "k8s.io/client-go/listers/apps/v1"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"

	// coreListers "k8s.io/client-go/listers/core/v1"
	"github.com/golang/glog"
	"github.com/kubernetes-incubator/apiserver-builder/pkg/builders"
	simpleyaml "github.com/smallfish/simpleyaml"
)

// +controller:group=asf,version=v1alpha1,kind=MicroservicesComponent,resource=microservicescomponents
type MicroservicesComponentControllerImpl struct {
	builders.DefaultControllerFns

	// lister indexes properties about MicroservicesComponent
	lister listers.MicroservicesComponentLister

	microserviceComponentSynced cache.InformerSynced
	deploymentsLister           v1.DeploymentLister
	statefulsetsLister          v1.StatefulSetLister
	namespacesSynced            cache.InformerSynced

	si                  *sharedinformers.SharedInformers
	kubernetesClientSet *kubernetes.Clientset
	catalogClientSet    *catalogclient.Clientset
	clientSet           clientset.Interface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

const (
	SuccessSynced = "Synced"
	// SuccessUpdated when project is updated successfully
	SuccessUpdated = "Updated"
	// SuccessCreated created success status
	SuccessCreated = "Created"
	// ErrCreating error while creating
	ErrCreating = "ErrCreating"
	// ErrSyncing error while syncing project
	ErrSyncing          = "ErrSyncing"
	controllerAgentName = "asf-microservicesComponent-controller"
	// MessageReleaseErrCreating message for release creation err
	// MessageProjectSyncErr message for syncing issue
	MessageReleaseCreated = "Release %s created successfully"
	// MessageReleaseErrCreating message for release creation err
	MessageReleaseErrCreating = "Error creating release %s: %s"
	//
	MessageProjectSyncErr = "Project sync error: %s"
)

// Init initializes the controller and is called by the generated code
// Register watches for additional resource types here.
func (c *MicroservicesComponentControllerImpl) Init(arguments sharedinformers.ControllerInitArguments) {
	// Use the lister for indexing microservicescomponents labels

	c.si = arguments.GetSharedInformers()
	c.lister = arguments.GetSharedInformers().Factory.Asf().V1alpha1().MicroservicesComponents().Lister()

	// register app watcher
	deploymentsInformer := arguments.GetSharedInformers().KubernetesFactory.Apps().V1().Deployments()
	c.deploymentsLister = deploymentsInformer.Lister()
	// c.deploymentsSynced = deploymentsInformer.Informer().HasSynced

	statefulsetsInformer := arguments.GetSharedInformers().KubernetesFactory.Apps().V1().StatefulSets()
	c.statefulsetsLister = statefulsetsInformer.Lister()
	// c.statefulsetsSynced = statefulsetsInformer.Informer().HasSynced

	// register asf component watcher
	// MicroservicesComponentsInformer := arguments.GetSharedInformers().Factory.Asf().V1alpha1().MicroservicesComponents()
	// c.MicroservicesComponentsLister = MicroservicesComponentsInformer.Lister()
	// c.MicroservicesComponentsSynced = MicroservicesComponentsInformer.Informer().HasSynced

	clientset, err := clientset.NewForConfig(arguments.GetRestConfig())
	if err != nil {
		glog.Fatalf("error creating machine client: %v", err)
	}
	catalogClientSet, err := catalogclient.NewForConfig(arguments.GetRestConfig())
	if err != nil {
		glog.Fatalf("error creating catalog client: %v", err)
	}

	c.clientSet = clientset
	c.catalogClientSet = catalogClientSet

	c.kubernetesClientSet = arguments.GetSharedInformers().KubernetesClientSet
	arguments.GetSharedInformers().Watch("WatchDeploy", deploymentsInformer.Informer(), nil, c.reconcileDeployments)

	arguments.GetSharedInformers().Watch("WatchStatefulset", statefulsetsInformer.Informer(), nil, c.reconcileStatefulSets)

	// arguments.GetSharedInformers().Watch("WatchPod", podInformer.Informer(), nil, c.reconcilePods)

	// setting events
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(glog.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: c.si.KubernetesClientSet.CoreV1().Events("")})
	c.recorder = eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})
}

// Reconcile handles enqueued messages
func (c *MicroservicesComponentControllerImpl) Reconcile(u *v1alpha1.MicroservicesComponent) error {
	// Implement controller logic here
	log.Printf("Running reconcile MicroservicesComponent for %s\n", u.Name)
	needUpdate := false
	needUpdateStatus := false
	v := u.Spec.RawValues
	oldRelease, _ := c.catalogClientSet.CatalogController().Releases(u.ObjectMeta.Namespace).Get(u.ObjectMeta.Name, metav1.GetOptions{})
	release := newRelease(u, oldRelease)

	u.Spec.ReleaseRef.Name = release.Name
	u.Spec.ReleaseRef.Namespace = release.Namespace
	c.clientSet.AsfV1alpha1().MicroservicesComponents(u.GetNamespace()).Update(u)

	env, err := c.clientSet.AsfV1alpha1().MicroservicesEnvironments().Get(u.Namespace, metav1.GetOptions{})
	if err != nil {
		return err
	}
	if v != "" {
		// Delete all resources in this component
		if oldRelease.Name == "" {
			needUpdate = true
			r, err := c.catalogClientSet.CatalogController().Releases(u.ObjectMeta.Namespace).Create(release)
			if err != nil {
				msg := fmt.Sprintf(MessageReleaseErrCreating, r.Name, err.Error())
				c.recorder.Event(u, corev1.EventTypeWarning, ErrCreating, msg)
				return err
			}
			msg := fmt.Sprintf(MessageReleaseCreated, r.Name)
			c.recorder.Event(u, corev1.EventTypeNormal, SuccessCreated, msg)
			u.Status.CurrentInstallStep = 1
			u.Status.Status = asfv1alpha1.StatusInstalling
		} else {
			_, err := c.catalogClientSet.CatalogController().Releases(u.ObjectMeta.Namespace).Update(release)
			u.Status.Status = asfv1alpha1.StatusPending
			if err != nil {
				return err
			}
		}
	}

	if u.Status.PodControllerInfo.Available < u.Status.PodControllerInfo.Unavailable {
		u.Status.Status = asfv1alpha1.StatusPending
		needUpdate = true
	}
	if u.Status.PodControllerInfo.Desired > 0 && u.Status.PodControllerInfo.Available == u.Status.PodControllerInfo.Desired {
		u.Status.Status = asfv1alpha1.StatusRunning
		needUpdate = true
		u.Status.CurrentInstallStep = 5
		if u.Spec.Order == 3 && u.Spec.Type == asfv1alpha1.MicroservicesEnvironmentCompnentTypeBasic {
			env.Status.Components.Status = asfv1alpha1.MicroservicesEnvironmentComponentsStatusBasicReady
			needUpdateStatus = true
		}
	}

	if needUpdateStatus {
		log.Printf("Start update status of envComponents %v \n", env.Status.Components)
		_, err = c.clientSet.AsfV1alpha1().MicroservicesEnvironments().UpdateStatus(env)
		if err != nil {
			return err
		}
	}

	if needUpdate || needUpdateStatus {
		_, err = c.clientSet.AsfV1alpha1().MicroservicesComponents(u.GetNamespace()).UpdateStatus(u)
		if err != nil {
			return err
		}

		for i, component := range env.Spec.MicroservicesComponentRefs {
			if component.Name == u.Name {
				env.Spec.MicroservicesComponentRefs[i].Status = u.Status.Status
				ingressHost, hostIP, err := c.handleService(env, u)
				if err == nil {
					env.Spec.MicroservicesComponentRefs[i].Host = &hostIP
					env.Spec.MicroservicesComponentRefs[i].IngressHost = &ingressHost
					env.Spec.MicroservicesComponentRefs[i].Order = u.Spec.Order

					if needUpdate {
						env.Spec.MicroservicesComponentRefs[i].SubValues, err = c.getCompSubValue(u)
					}
				} else {
					err = fmt.Errorf("Failed to lookup services for %v: %v", true, err)
					log.Printf("%v", err)
				}
			}
		}

		env, err = c.clientSet.AsfV1alpha1().MicroservicesEnvironments().Update(env)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *MicroservicesComponentControllerImpl) Get(namespace, name string) (*v1alpha1.MicroservicesComponent, error) {
	return c.lister.MicroservicesComponents(namespace).Get(name)
}

// reconcileDeployments is serialized by an internal queue. It has built-in rated limited retry logic.
// So as long as the reconcile loop return an error, the processing queue will retry the
// reconciliation.
func (c *MicroservicesComponentControllerImpl) reconcileDeployments(key string) error {
	log.Printf("Running reconcile reconcileDeployments for key %s \n", key)

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		return err
	}
	deploy, err := c.deploymentsLister.Deployments(namespace).Get(name)
	if errors.IsNotFound(err) {
		return err
	}
	if err != nil {
		glog.Errorf("Unable to retrieve Node %v from store: %v", key, err)
		return err
	}
	log.Printf("Running reconcile reconcileDeployments for %s -- %s \n", namespace, name)

	for _, ownerRef := range deploy.OwnerReferences {
		if ownerRef.Kind == asfv1alpha1.CatalogKind {
			releaseName := deploy.GetLabels()["release"]
			if _, v := asfv1alpha1.CompInfoMap[releaseName]; v {
				if msComp, err := c.Get(namespace, releaseName); err == nil && msComp != nil {
					isExist := false // is the deployment a new resource or old version
					for _, ref := range msComp.Spec.DeploymentRefs {
						if ref.Name == name && ref.Namespace == namespace {
							isExist = true
							break
						}
					}

					if isExist == false || len(msComp.Spec.DeploymentRefs) == 0 {
						msComp.Spec.DeploymentRefs = append(msComp.Spec.DeploymentRefs,
							asfv1alpha1.ResourceRef{
								Name:      name,
								Namespace: namespace,
							})
					}

					msComp.Status.PodControllerInfo.Available = deploy.Status.AvailableReplicas
					msComp.Status.PodControllerInfo.Desired = *deploy.Spec.Replicas
					msComp.Status.PodControllerInfo.Unavailable = deploy.Status.UnavailableReplicas
					msComp.Status.PodControllerInfo.Pending = deploy.Status.Replicas - deploy.Status.ReadyReplicas
					// msComp.Status.Conditions = deploy.Status.Conditions
					statusBytes, _ := json.Marshal(deploy.Status.Conditions)
					json.Unmarshal(statusBytes, &deploy.Status.Conditions)
					if deploy.Status.Replicas == 0 {
						msComp.Status.Status = asfv1alpha1.StatusStopped
					}
					if deploy.Status.Replicas > 0 && deploy.Status.ReadyReplicas == 0 {
						msComp.Status.Status = asfv1alpha1.StatusPending
						// not stopped
						if msComp.Status.CurrentInstallStep != 5 {
							msComp.Status.CurrentInstallStep = 3
						}
					}
					if deploy.Status.ReadyReplicas < deploy.Status.Replicas && deploy.Status.ReadyReplicas > 0 && msComp.Status.CurrentInstallStep == 3 {
						msComp.Status.CurrentInstallStep = 4
					}
					_, err := c.clientSet.AsfV1alpha1().MicroservicesComponents(msComp.GetNamespace()).UpdateStatus(msComp)
					if err != nil {
						return err
					}
					env, err := c.clientSet.AsfV1alpha1().MicroservicesEnvironments().Get(msComp.Namespace, metav1.GetOptions{})
					if err != nil {
						return err
					}
					for i, component := range env.Spec.MicroservicesComponentRefs {
						if component.Name == msComp.Name {
							env.Spec.MicroservicesComponentRefs[i].Status = msComp.Status.Status
						}
					}
					_, err = c.clientSet.AsfV1alpha1().MicroservicesEnvironments().Update(env)
					if err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}

// reconcileDeployments is serialized by an internal queue. It has built-in rated limited retry logic.
// So as long as the reconcile loop return an error, the processing queue will retry the
// reconciliation.
func (c *MicroservicesComponentControllerImpl) reconcileStatefulSets(key string) error {
	log.Printf("Running reconcile reconcileStatefulSets for key %s \n", key)

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		return err
	}
	stateful, err := c.statefulsetsLister.StatefulSets(namespace).Get(name)
	if errors.IsNotFound(err) {
		return err
	}
	if err != nil {
		glog.Errorf("Unable to retrieve Node %v from store: %v", key, err)
		return err
	}
	log.Printf("Running reconcile reconcileStatefulSets for %s -- %s \n", namespace, name)
	for _, ownerRef := range stateful.OwnerReferences {
		if ownerRef.Kind == asfv1alpha1.CatalogKind {
			releaseName := stateful.GetLabels()["release"]
			if _, v := asfv1alpha1.CompInfoMap[releaseName]; v {
				if msComp, err := c.Get(namespace, releaseName); err == nil && msComp != nil {
					isExist := false // is the statefulset a new resource or old version
					for _, ref := range msComp.Spec.StatefulSetRefs {
						if ref.Name == name && ref.Namespace == namespace {
							isExist = true
							break
						}
					}
					if isExist == false || len(msComp.Spec.StatefulSetRefs) == 0 {
						msComp.Spec.StatefulSetRefs = append(msComp.Spec.StatefulSetRefs,
							asfv1alpha1.ResourceRef{
								Name:      name,
								Namespace: namespace,
							})
					}

					msComp.Status.PodControllerInfo.Available = stateful.Status.ReadyReplicas
					msComp.Status.PodControllerInfo.Desired = *stateful.Spec.Replicas
					msComp.Status.PodControllerInfo.Unavailable = stateful.Status.CurrentReplicas - stateful.Status.ReadyReplicas
					msComp.Status.PodControllerInfo.Pending = stateful.Status.Replicas - stateful.Status.CurrentReplicas
					statusBytes, _ := json.Marshal(stateful.Status.Conditions)
					json.Unmarshal(statusBytes, &msComp.Status.Conditions)
					if *stateful.Spec.Replicas == 0 {
						msComp.Status.Status = asfv1alpha1.StatusStopped
					}
					if *stateful.Spec.Replicas > 0 && stateful.Status.ReadyReplicas == 0 {
						msComp.Status.Status = asfv1alpha1.StatusPending
						// not stopped
						if msComp.Status.CurrentInstallStep != 5 {
							msComp.Status.CurrentInstallStep = 3
						}
					}

					if stateful.Status.ReadyReplicas < stateful.Status.Replicas && stateful.Status.ReadyReplicas > 0 && msComp.Status.CurrentInstallStep == 3 {
						msComp.Status.CurrentInstallStep = 4
					}
					_, err := c.clientSet.AsfV1alpha1().MicroservicesComponents(msComp.GetNamespace()).UpdateStatus(msComp)
					if err != nil {
						return err
					}
					env, err := c.clientSet.AsfV1alpha1().MicroservicesEnvironments().Get(msComp.Namespace, metav1.GetOptions{})
					if err != nil {
						return err
					}
					for i, component := range env.Spec.MicroservicesComponentRefs {
						if component.Name == msComp.Name {
							env.Spec.MicroservicesComponentRefs[i].Status = msComp.Status.Status
						}
					}
					_, err = c.clientSet.AsfV1alpha1().MicroservicesEnvironments().Update(env)
					if err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}

func newRelease(component *asfv1alpha1.MicroservicesComponent, o *catav1alpha1.Release) *catav1alpha1.Release {
	labels := map[string]string{
		asfv1alpha1.AnnotationsKeyMicroservicesComponent: component.ObjectMeta.Name,
	}

	newRelease := &catav1alpha1.Release{
		ObjectMeta: metav1.ObjectMeta{
			Name:      component.ObjectMeta.Name,
			Labels:    labels,
			Namespace: component.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(component, schema.GroupVersionKind{
					Group:   asfv1alpha1.SchemeGroupVersion.Group,
					Version: asfv1alpha1.SchemeGroupVersion.Version,
					Kind:    asfv1alpha1.MicroservicesComponentKind,
				}),
			},
		},
		Spec: catav1alpha1.ReleaseSpec{
			Chart: catav1alpha1.ReleaseSpecChart{
				Name:    component.Name,
				Version: component.Name,
			},
			RawValues: component.Spec.RawValues,
			Values: []catav1alpha1.ReleaseSpeValue{
				catav1alpha1.ReleaseSpeValue{
					DisplayName: catav1alpha1.I18NString{},
					Items:       []catav1alpha1.ReleaseSpecValueItem{},
				},
			},
			Resources: []catav1alpha1.ReleaseSpecResource{
				catav1alpha1.ReleaseSpecResource{},
			},
		},
	}

	if o.ObjectMeta.ResourceVersion != "" {
		newRelease.ObjectMeta.ResourceVersion = o.ObjectMeta.ResourceVersion
	}
	return newRelease
}

func (c *MicroservicesComponentControllerImpl) reconcilePods(key string) error {
	return nil
}

func (c *MicroservicesComponentControllerImpl) handleService(u *asfv1alpha1.MicroservicesEnvironment, compoent *asfv1alpha1.MicroservicesComponent) (string, string, error) {

	ingressHost, hostIP := "", ""

	s := c.si.KubernetesClientSet.CoreV1().Services(u.Name)

	ings := c.si.KubernetesClientSet.ExtensionsV1beta1().Ingresses(u.Name)

	appLabel := "release=" + compoent.Spec.ReleaseRef.Name
	log.Printf("Running handleservice for %s  %s %s\n", u.Name, compoent.Spec.ReleaseRef.Name, appLabel)

	inglist, err := ings.List(metav1.ListOptions{
		LabelSelector: appLabel,
	})

	if err != nil && !errors.IsNotFound(err) {
		log.Printf("Failed to lookup ingress for %s: %v", appLabel, err)
		return "", "", err
	}

	if len(inglist.Items) != 0 {

		for _, ing := range inglist.Items {

			rule := ing.Spec.Rules[0]
			ingressHost = fmt.Sprintf("http://%s%s", rule.Host, rule.HTTP.Paths[0].Path)
		}

	}

	sList, err := s.List(metav1.ListOptions{
		LabelSelector: appLabel,
	})

	if err != nil {
		//err = fmt.Errorf("Failed to lookup services for %s: %v", appLabel, err)
		log.Printf("Failed to lookup services for %s: %v", appLabel, err)
		return "", "", err
	}

	if len(sList.Items) != 0 {
		for _, service := range sList.Items {

			//log.Printf("found service for %s  %v\n", compoent.Spec.ReleaseRef.Name, service)

			if service.Spec.ClusterIP != "None" && service.Spec.ClusterIP != "" {
				port := fmt.Sprint(service.Spec.Ports[0].Port)

				hostIP = "http://" + service.Spec.ClusterIP + ":" + port

			}

		}
	}

	return ingressHost, hostIP, nil

}

func (c *MicroservicesComponentControllerImpl) getCompSubValue(comp *asfv1alpha1.MicroservicesComponent) (*map[string]*runtime.RawExtension, error) {
	yaml, err := c.parseValidRawValue(comp.Spec.RawValues)
	var rawExK []byte
	if err != nil {
		return nil, err
	}
	subValueRM := make(map[string]*runtime.RawExtension)
	subValueKey2Path := asfv1alpha1.CompInfoMap[comp.Name].SubValueKey2Path
	for k, pathStr := range subValueKey2Path {
		pathInterface := make([]interface{}, len(pathStr))
		for idx, val := range pathStr {
			pathInterface[idx] = val
		}
		goal := yaml.GetPath(pathInterface...)
		if goal.IsFound() {
			if goal.IsArray() {
				arr, _ := goal.Array()
				rawExK, err = json.Marshal(arr)
			} else if goal.IsMap() {
				m, _ := goal.Map()
				rawExK, err = json.Marshal(m)
			} else {
				str, _ := goal.String()
				rawExK, err = json.Marshal(str)
			}
			if err != nil {
				glog.Errorf("Json Marshal error%v of comp %s-%s", err, comp.Name, comp.Namespace)
				return nil, err
			}
			subValueRM[k] = &runtime.RawExtension{Raw: rawExK}
		}
	}

	return &subValueRM, nil
}

func (c *MicroservicesComponentControllerImpl) parseValidRawValue(validRawValue string) (*simpleyaml.Yaml, error) {
	source := []byte(validRawValue)
	return simpleyaml.NewYaml(source)
}
