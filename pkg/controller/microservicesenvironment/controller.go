/*
Copyright 2018 Alauda.io.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package microservicesenvironment

import (
	"fmt"
	"log"

	"k8s.io/apimachinery/pkg/util/runtime"

	"alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	"k8s.io/client-go/kubernetes"

	"k8s.io/client-go/tools/record"

	"alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset/scheme"

	"github.com/golang/glog"
	"github.com/kubernetes-incubator/apiserver-builder/pkg/builders"

	asfv1alpha1 "alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	listers "alauda.io/asf-apiserver/pkg/client/listers_generated/asf/v1alpha1"
	"alauda.io/asf-apiserver/pkg/controller/sharedinformers"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	coreListers "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
)

const controllerAgentName = "asf-microservicesEnvironment-controller"

const (
	// SuccessSynced is used as part of the Event 'reason' when a Project is synced
	SuccessSynced = "Synced"
	// SuccessUpdated when project is updated successfully
	SuccessUpdated = "Updated"
	// SuccessCreated created success status
	SuccessCreated = "Created"
	// ErrCreating error while creating
	ErrCreating = "ErrCreating"
	// ErrSyncing error while syncing project
	ErrSyncing = "ErrSyncing"
	// ErrResourceExists is used as part of the Event 'reason' when a Project fails
	// to sync due to a Namespace of the same name already existing.
	ErrResourceExists = "ErrResourceExists"
	// ErrProjectSpecInvalid error when controller finds an invalid project spec
	ErrProjectSpecInvalid = "ErrProjectSpecInvalid"

	ComponentStatusCreated = "Created"
	ComponentStatusReady   = "Ready"
	ComponentStatusUnknow  = "Unknow"

	// MessageResourceExists is the message used for Events when a resource
	// fails to sync due to a Deployment already existing
	MessageResourceExists = "Resource %q already exists and is not managed by Project"
	// MessageResourceSynced is the message used for an Event fired when a Project
	// is synced successfully
	MessageResourceSynced = "Project synced successfully"
	// MessageProjectSpecInvalid message content for the invalid project spec
	MessageProjectSpecInvalid = "Project's spec is invalid"
	// MessageProjectUpdated event mesage for when a full project update was performed
	MessageProjectUpdated = "Project updated successfully"
	// MessageNamespaceCreated message of success creating a namespace
	MessageNamespaceCreated = "Namespace %s created successfully"
	// MessageNamespaceErrCreating message for namespace creation err
	MessageNamespaceErrCreating = "Error creating namespace %s: %s"
	// MessageProjectSyncErr message for syncing issue
	MessageProjectSyncErr = "Project sync error: %s"

	//Secrets
	SecretName = "mysecret"
	SecretData = `{"auths":{"index.alauda.cn":{"username":"alaudak8s/push2","password":"123456","email":"devs@alauda.io","auth":"YWxhdWRhazhzL3B1c2gyOjEyMzQ1Ng=="}}}`
)

// +controller:group=asf,version=v1alpha1,kind=MicroservicesEnvironment,resource=microservicesenvironments
type MicroservicesEnvironmentControllerImpl struct {
	builders.DefaultControllerFns

	// lister indexes properties about MicroservicesEnvironment
	microserviceEnvironmentLister listers.MicroservicesEnvironmentLister
	microserviceEnvironmentSynced cache.InformerSynced

	// Data sync for namespace
	namespacesLister coreListers.NamespaceLister
	namespacesSynced cache.InformerSynced

	si *sharedinformers.SharedInformers

	kubernetesClientSet *kubernetes.Clientset
	clientSet           clientset.Interface

	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// Init initializes the controller and is called by the generated code
// Register watches for additional resource types here.
func (c *MicroservicesEnvironmentControllerImpl) Init(arguments sharedinformers.ControllerInitArguments) {
	// **** kubernetes resources
	c.si = arguments.GetSharedInformers()
	// namespace
	namespaceInformer := arguments.GetSharedInformers().KubernetesFactory.Core().V1().Namespaces()
	c.namespacesLister = namespaceInformer.Lister()
	c.namespacesSynced = namespaceInformer.Informer().HasSynced

	// **** asf resources
	//  environments
	microserviceEnvironmentInformer := arguments.GetSharedInformers().Factory.Asf().V1alpha1().MicroservicesEnvironments()
	c.microserviceEnvironmentLister = microserviceEnvironmentInformer.Lister()
	c.microserviceEnvironmentSynced = microserviceEnvironmentInformer.Informer().HasSynced

	clientset, err := clientset.NewForConfig(arguments.GetRestConfig())
	if err != nil {
		glog.Fatalf("error creating machine client: %v", err)
	}
	c.clientSet = clientset
	c.kubernetesClientSet = arguments.GetSharedInformers().KubernetesClientSet

	arguments.GetSharedInformers().Watch("WatchNamespace", namespaceInformer.Informer(), nil, c.reconcileNamespace)

	// setting events
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(glog.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: c.si.KubernetesClientSet.CoreV1().Events("")})
	c.recorder = eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})
	// projects
	microserviceEnvironmentInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		DeleteFunc: func(obj interface{}) {
			// It means the project has been deleted if come here.
			go func() {
				var (
					microserviceEnv *asfv1alpha1.MicroservicesEnvironment
					ok              bool
				)

				if microserviceEnv, ok = obj.(*asfv1alpha1.MicroservicesEnvironment); !ok {
					glog.Errorf("The format of deleted microserviceEnv is invalid. value: %v", microserviceEnv)
					return
				}

				c.DeleteMicroserviceEnv(microserviceEnv)
			}()
		},
	})

	/*
		// namespaces
		// we only deal with namespaces that were created by projects
		// and ignore any non-updated namespaces
		namespaceInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
			AddFunc: c.handleObject,
			UpdateFunc: func(old, new interface{}) {
				newNamespace := new.(*corev1.Namespace)
				oldNamespace := old.(*corev1.Namespace)
				if newNamespace.ResourceVersion == oldNamespace.ResourceVersion {
					// Periodic resync will send update events for all known Namespaces.
					// Two different versions of the same Namespace will always have different RVs.
					return
				}
				c.handleObject(new)
			},
			DeleteFunc: c.handleObject,
		})
	*/

}

// Reconcile handles enqueued messages
func (c *MicroservicesEnvironmentControllerImpl) Reconcile(u *asfv1alpha1.MicroservicesEnvironment) error {
	// Implement controller logic here
	log.Printf("Running reconcile MicroservicesEnvironment for %s\n", u.Name)

	needsUpdate := false
	isReady := true

	CompletelyReady := false

	n := u.Spec.Namespace
	if n == nil {
		needsUpdate = true
		u.Spec.Namespace = &asfv1alpha1.MicroserviceEnvironmentNamespace{
			Name: u.Name,
			Type: asfv1alpha1.MicroservicesEnvironmentNamespaceTypeOwned,
		}
		n = u.Spec.Namespace
	}
	if u.Status.Namespace == nil {
		needsUpdate = true
		u.Status.Namespace = &asfv1alpha1.MicroservicesEnvironmentNamespaceStatus{
			Name:   n.Name,
			Status: asfv1alpha1.MicroservicesEnvironmentNamespaceStatusUnknown,
		}
	}

	if u.Status.Components == nil {
		needsUpdate = true
		u.Status.Components = &asfv1alpha1.MicroservicesEnvironmentComponentsStatus{
			Status: asfv1alpha1.MicroservicesEnvironmentComponentsStatusUnknown,
		}
	}

	ns, err := c.si.KubernetesClientSet.CoreV1().Namespaces().Get(n.Name, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		glog.V(4).Infof("Namespace %v does not exist. Will create now", n.Name)

		ns, err = c.si.KubernetesClientSet.CoreV1().Namespaces().Create(newNamespace(u))

		if ns != nil {
			// creating an event saying that our namespace was created
			msg := fmt.Sprintf(MessageNamespaceCreated, ns.Name)
			c.recorder.Event(u, corev1.EventTypeNormal, SuccessCreated, msg)
			needsUpdate = true

			u.Status.Namespace.Status = asfv1alpha1.MicroservicesEnvironmentNamespaceStatusReady

		} else {
			isReady = false
			glog.Errorf("Namespace %v create error: %v", n.Name, err)
			msg := fmt.Sprintf(MessageNamespaceErrCreating, n.Name, err.Error())
			c.recorder.Event(u, corev1.EventTypeWarning, ErrCreating, msg)
			needsUpdate = true
			u.Status.Namespace.Status = asfv1alpha1.MicroservicesEnvironmentNamespaceStatusError
		}
	} else if err != nil {

		glog.Errorf("Unable to retrieve namespace %v from store: %v", n.Name, err)
		needsUpdate = true
		isReady = false
		u.Status.Namespace.Status = asfv1alpha1.MicroservicesEnvironmentNamespaceStatusError
		_, updateErr := c.clientSet.AsfV1alpha1().MicroservicesEnvironments().UpdateStatus(u)
		if updateErr != nil {
			runtime.HandleError(updateErr)
		}

		return err
	} else if err == nil && ns != nil {
		// namespace exists
		if u.Status.Namespace.Status != asfv1alpha1.MicroservicesEnvironmentNamespaceStatusReady {
			needsUpdate = true
			u.Status.Namespace.Status = asfv1alpha1.MicroservicesEnvironmentNamespaceStatusReady
		}
	}

	// namespace is ready ,create secret
	if u.Status.Namespace.Status == asfv1alpha1.MicroservicesEnvironmentNamespaceStatusReady {
		c.createNamespaceSecret(u.Name)
	}

	// components
	if u.Spec.MicroservicesComponentRefs != nil && len(u.Spec.MicroservicesComponentRefs) > 0 {

		componentInformer := c.si.Factory.Asf().V1alpha1().MicroservicesComponents()
		componentLister := componentInformer.Lister()

		for i, component := range u.Spec.MicroservicesComponentRefs {

			_, err := componentLister.MicroservicesComponents(u.Name).Get(component.Name)
			if err != nil && errors.IsNotFound(err) {
				microComponent := newComponent(&component, u)
				//c..Asf().V1alpha1().MicroservicesComponents().Create(microComponent)
				mc, err := c.clientSet.AsfV1alpha1().MicroservicesComponents(u.Name).Create(microComponent)

				if err == nil && mc != nil {
					// create component successfully
					needsUpdate = true
					u.Spec.MicroservicesComponentRefs[i].Status = ComponentStatusCreated
				}
				if err != nil {
					glog.Errorf("Unable to create CRD MicroservicesComponent %v with err : %v", microComponent, err)
					needsUpdate = true
					isReady = false
					u.Spec.MicroservicesComponentRefs[i].Status = ComponentStatusUnknow

					runtime.HandleError(err)
				}

			}

			// if existCompont != nil {
			// 		needsUpdate = true
			// 		u.Spec.MicroservicesComponentRefs[i].Status = existCompont.Status.Status

			// 		// component already exist

			// 		if existCompont.Status.Status == asfv1alpha1.StatusRunning {
			// 			if IsBasicComponent(component.Name) {
			// 				basicReady = basicReady && true
			// 			}

			// 			CompletelyReady = CompletelyReady && true

			// handle service

			//
			// 		} else {
			// 			if IsBasicComponent(component.Name) {
			// 				basicReady = basicReady && false
			// 			}

			// 			CompletelyReady = CompletelyReady && false

			// 		}

			// }

		}

	}
	if isReady {
		u.Status.Status = "Ready"
	} else {
		u.Status.Status = "NotReady"
	}

	if CompletelyReady {
		needsUpdate = true
		u.Status.Components.Status = asfv1alpha1.MicroservicesEnvironmentComponentsStatusCompletelyReady
	}

	if needsUpdate {
		c.clientSet.AsfV1alpha1().MicroservicesEnvironments().Update(u)
		c.clientSet.AsfV1alpha1().MicroservicesEnvironments().UpdateStatus(u)

	}

	return nil
}

func (c *MicroservicesEnvironmentControllerImpl) Get(namespace, name string) (*asfv1alpha1.MicroservicesEnvironment, error) {
	return c.microserviceEnvironmentLister.Get(name)
}

func (c *MicroservicesEnvironmentControllerImpl) DeleteMicroserviceEnv(microserviceEnv *asfv1alpha1.MicroservicesEnvironment) (err error) {
	glog.Infof("Delete the project %v", microserviceEnv)
	if microserviceEnv.Spec.Namespace != nil {
		microserviceEnvNamespace := microserviceEnv.Spec.Namespace
		//listOptions := metav1.ListOptions{}
		// Delete all resources in this namespace

		err = c.si.KubernetesClientSet.CoreV1().Namespaces().Delete(microserviceEnvNamespace.Name, &metav1.DeleteOptions{})
		glog.V(5).Infof("The result of namespace delete: %v", err)

	}
	return
}

// reconcileNamespace is serialized by an internal queue. It has built-in rated limited retry logic.
// So as long as the reconcile loop return an error, the processing queue will retry the
// reconciliation.
func (c *MicroservicesEnvironmentControllerImpl) reconcileNamespace(key string) error {
	_, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		return err
	}
	//c.namespacesLister.Get
	n, err := c.si.KubernetesClientSet.CoreV1().Namespaces().Get(name, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		return err
	}
	if err != nil {
		glog.Errorf("Unable to retrieve Node %v from store: %v", key, err)
		return err
	}

	for _, ownerRef := range n.OwnerReferences {
		if ownerRef.Kind == asfv1alpha1.MicroservicesEnvironmentKind {
			if n.DeletionTimestamp.IsZero() {
				// create or update
				return nil
			} else {
				// delete
				return nil
			}
			if msEnv, err := c.Get(n.Name, ownerRef.Name); err == nil && msEnv != nil {

			}
		}
	}

	return nil

}

// newNamespace creates a new Namespace for a Project resource. It also sets
// the appropriate OwnerReferences on the resource so handleObject can discover
// the Project resource that 'owns' it.
func newNamespace(msEnv *asfv1alpha1.MicroservicesEnvironment) *corev1.Namespace {
	labels := map[string]string{
		asfv1alpha1.AnnotationsKeyMicroservicesEnvironment: msEnv.Name,
	}
	return &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   msEnv.Name,
			Labels: labels,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(msEnv, schema.GroupVersionKind{
					Group:   asfv1alpha1.SchemeGroupVersion.Group,
					Version: asfv1alpha1.SchemeGroupVersion.Version,
					Kind:    asfv1alpha1.MicroservicesEnvironmentKind,
				}),
			},
		},
	}
}

func objectRef(node *corev1.Node) *corev1.ObjectReference {

	return &corev1.ObjectReference{
		Kind:      "Node",
		Namespace: node.ObjectMeta.Namespace,
		Name:      node.ObjectMeta.Name,
	}
}

func newComponent(component *asfv1alpha1.MicroservicesComponentRef, msEnv *asfv1alpha1.MicroservicesEnvironment) *asfv1alpha1.MicroservicesComponent {
	labels := map[string]string{
		asfv1alpha1.AnnotationsKeyMicroservicesEnvironment: component.Name,
	}

	return &asfv1alpha1.MicroservicesComponent{
		ObjectMeta: metav1.ObjectMeta{
			Name:      component.Name,
			Labels:    labels,
			Namespace: msEnv.Name,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(msEnv, schema.GroupVersionKind{
					Group:   asfv1alpha1.SchemeGroupVersion.Group,
					Version: asfv1alpha1.SchemeGroupVersion.Version,
					Kind:    asfv1alpha1.MicroservicesEnvironmentKind,
				}),
			},
		},
		Spec: asfv1alpha1.MicroservicesComponentSpec{
			MicroservicesEnvironmentName: msEnv.Name,
			ChartName:                    component.Name,
		},
	}
}

// Create namespace Docker json config secret
func (c *MicroservicesEnvironmentControllerImpl) createNamespaceSecret(nsName string) error {

	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: SecretName,
		},
		StringData: map[string]string{
			corev1.DockerConfigJsonKey: SecretData,
		},
		Type: corev1.SecretTypeDockerConfigJson,
	}

	_, err := c.si.KubernetesClientSet.CoreV1().Secrets(nsName).Get(SecretName, metav1.GetOptions{})
	if err != nil {
		glog.Infof("Creating namespace [%s] secret [%s]\n", nsName, SecretName)
		_, err = c.si.KubernetesClientSet.CoreV1().Secrets(nsName).Create(secret)

		if err != nil {

			glog.Errorf("Unable to create secret %v  with errs : %v", secret, err)
			return err
		}
	}
	return nil
}

func IsBasicComponent(componentName string) bool {

	basics := []string{"zookeeper", "kafka", "config-server", "eureka"}

	for _, basic := range basics {
		if basic == componentName {

			return true
		}
	}

	return false

}
