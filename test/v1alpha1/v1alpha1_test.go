package v1alpha1_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	catalogclient "catalog-controller/pkg/client/clientset/versioned"

	clientset "alauda.io/asf-apiserver/pkg/client/clientset_generated/clientset"
	asfinformers "alauda.io/asf-apiserver/pkg/client/informers_generated/externalversions"
	"github.com/fatih/color"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	toDelete           = []string{}
	yellow             = color.New(color.FgYellow).Add(color.Bold)
	blue               = color.New(color.FgBlue).Add(color.Bold)
	white              = color.New(color.FgWhite)
	red                = color.New(color.FgRed)
	stoutBuf, sterrBuf bytes.Buffer
	asfClient          clientset.Interface
	catalogClient	   catalogclient.Interface
	k8sClient          kubernetes.Interface
	kubeInformers      kubeinformers.SharedInformerFactory
	asfInformers    asfinformers.SharedInformerFactory
	stopChan           chan struct{}
)

func Start() {
	if kubeInformers != nil {
		go kubeInformers.Start(stopChan)
	}
	if asfInformers != nil {
		go asfInformers.Start(stopChan)
	}

}

func addDelete(file string) {
	toDelete = append(toDelete, file)
}

func makeFile(fromFile string, data map[string]string) (toFile string) {
	b, _ := ioutil.ReadFile(fromFile)
	content := string(b)
	for k, v := range data {
		content = strings.Replace(content, k, v, -1)
	}
	s := strings.Split(fromFile, "/")
	fileName := s[len(s)-1]
	_, err := os.Stat("data")
	if os.IsNotExist(err) {
		os.Mkdir("data", os.ModePerm)
	}
	toFile = "data/" + fileName
	d1 := []byte(content)
	ioutil.WriteFile(toFile, d1, 0644)
	toDelete = append(toDelete, toFile)
	return toFile
}

func TestV1Alpha1(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("../v1alpha1.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "DevopsV1alpha1", []Reporter{junitReporter})
	close(stopChan)
}

func kubectlApply(fileName string, verbose ...bool) (output []byte, err error) {
	printH(line34, "kubectl apply ", fileName)
	output, err = commandP(
		"kubectl",
		"apply",
		"-f",
		fileName,
		"--validate=false",
	)
	if len(verbose) > 0 && verbose[0] && err != nil {
		printR("kubectl apply err:", err)
	}
	return
}

func command(name string, args ...string) ([]byte, error) {
	sterrBuf.Reset()
	defer func() {
		if sterrBuf.Len() > 0 {
			red.Println(mainLine)
			red.Println(errorTitle, strings.TrimSpace(sterrBuf.String()))
			red.Println(mainLine)
		}
	}()
	cmd := exec.Command(name, args...)
	cmd.Stderr = &sterrBuf
	return cmd.Output()
}

func commandP(name string, args ...string) (out []byte, err error) {
	out, err = command(name, args...)
	printR("out:", trim(out))
	if err != nil {
		printR("err:", err)
	}
	return
}

const (
	header = iota
	mainHeader
	result
	footer

	line       = `----------------------------------------------`
	line34     = `--------`
	mainLine   = `===================================================`
	errorTitle = " - Command returned error:"
)

func printH(content ...interface{}) {
	printTest(header, content...)
}
func printM(content ...interface{}) {
	printTest(mainHeader, content...)
}

func printF(content ...interface{}) {
	printTest(footer, content...)
}

func printR(content ...interface{}) {
	printTest(result, content...)
}

func trim(data []byte) string {
	return strings.TrimSpace(string(data))
}

func printTest(printType int, content ...interface{}) {
	switch printType {
	case mainHeader:
		blue.Println(mainLine)
		blue.Println(content...)
		blue.Println(mainLine)
	case header:
		fallthrough
	case footer:
		yellow.Println(content...)
	case result:
		fallthrough
	default:
		white.Println(content...)
	}
}

// DescribeF small set of cases
func DescribeF(title string, test func()) bool {
	return Describe(title, func() {
		printH(line34, "Starting:", title)
		test()
	})
}

// XDescribeF Pending tests
func XDescribeF(title string, test func()) bool {
	return XDescribe(title, func() {
		printH(line34, "Starting:", title)
		test()
	})
}

// DescribeM
// this is a major describe function to group a big set of cases
func DescribeM(title string, test func()) bool {
	return Describe(title, func() {
		defer GinkgoRecover()
		printM("\tStarting:", title)
		test()
		// printM("\tEnding:", title)
	})
}

var _ = AfterSuite(func() {
	printM("End!")
	var (
		err    error
		output []byte
	)
	for _, de := range toDelete {
		// printF("\ndeleting file:", de)
		output, err = exec.Command("kubectl", "delete", "-f", de).Output()
		if output != nil || err != nil {
			//
		}
		// GinkgoWriter.Write(output)
		// printF("output", string(output), "err", err)
		os.Remove(de)
	}
	os.Remove("data")
	// Expect(err).NotTo(HaveOccurred())

})

func init() {

	stopChan = make(chan struct{})
	var (
		clientConfig   *restclient.Config
		kubeConfigPath = ""
		err            error
		home           = os.Getenv("HOME")
		// kube           = os.Getenv("KUBE")
	)
	if _, err = os.Stat(fmt.Sprintf("%s/.kube/config", home)); err == nil {
		kubeConfigPath = fmt.Sprintf("%s/.kube/config", home)
		printH("Will use kube config: ", kubeConfigPath)
	}

	if clientConfig, err = clientcmd.BuildConfigFromFlags("", kubeConfigPath); err != nil {
		printR("Error building config:", err.Error())
	}
	if k8sClient, err = kubernetes.NewForConfig(clientConfig); err != nil {
		printR("Error building kubernetes client:", err.Error())
	}
	if asfClient, err = clientset.NewForConfig(clientConfig); err != nil {
		printR("Error building asf client:", err.Error())
	}
	if catalogClient, err = catalogclient.NewForConfig(clientConfig); err != nil {
		printR("Error building asf client:", err.Error())
	}
	kubeInformers = kubeinformers.NewSharedInformerFactory(k8sClient, time.Second*30)
	asfInformers = asfinformers.NewSharedInformerFactory(asfClient, time.Second*30)

}

func RetryExecution(f func() error) (err error) {
	count := 0
	for count < 5 {
		err = f()
		if err == nil {
			return
		}
	}
	return
}
