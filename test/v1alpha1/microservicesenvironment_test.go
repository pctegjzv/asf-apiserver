package v1alpha1_test

import (
	"fmt"
	// "strings"
	"time"
	
	"alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = DescribeM("MicroserviceEnvironment", func() {
	var (
		microservicesEnvironment          *v1alpha1.MicroservicesEnvironment
		microservicesEnvironmentList      *v1alpha1.MicroservicesEnvironmentList
		err                               error
		name                              string
		displayName                       string
		description                       string
		file                              string
		asfData                           map[string]string
	)
	BeforeEach(func() {
		name = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		displayName = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		description = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())

		
		file = "microservicesenvironment/test-microservicesenvironment.yaml"
		asfData = map[string]string{
			"${name}":        name,
			"${displayName}": displayName,
			"${description}": description,
		}
		file = makeFile(file, asfData) 
		kubectlApply(file, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
	})
	DescribeF("L1:AldDevops-2491: Get micro services environment according to name", func() {
		JustBeforeEach(func() {
		
			microservicesEnvironment, err = asfClient.AsfV1alpha1().MicroservicesEnvironments().Get(name, v1.GetOptions{})
			time.Sleep(time.Second * 1)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(microservicesEnvironment.GetName()).To(Equal(name))
		})
	})


	DescribeF("L1:AldDevops-2492: Get micro services environment list", func() {
		JustBeforeEach(func() {
			microservicesEnvironmentList, err = asfClient.AsfV1alpha1().MicroservicesEnvironments().List(v1.ListOptions{})
		})
		It("should contain "+name, func() {
			Expect(err).To(BeNil())
			Expect(microservicesEnvironmentList).ToNot(BeNil())
			Expect(microservicesEnvironmentList.Items).ToNot(BeEmpty())
			found := false
			for _, p := range microservicesEnvironmentList.Items {
				if p.Name == name {
					found = true
					break
				}
			}
			Expect(found).To(BeTrue())
		})
	})
})
