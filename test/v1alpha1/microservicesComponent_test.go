package v1alpha1_test
import s "strings"

import (
	"encoding/json"
	"fmt"
	"time"
	catav1 "catalog-controller/pkg/apis/catalogcontroller/v1alpha1"
	"alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func waitMicroserviceEnvironmentChangeToStatus(namespace string, microservicesComponentName string, expectStatus string) (result string) {
	RegisterFailHandler(Fail)
	timer := time.NewTimer(time.Second * 60)
	for {
		select {
		case <-timer.C:
			result = "timeout"
			return result
		default:
		    // kubectl get MicroservicesComponent zookeeper -n e2etestasfenvironment1533118253619817111 -o yaml
			outputDetail, _ := commandP("kubectl", "get", "MicroservicesComponent", microservicesComponentName, "-n", namespace,  "-o", "json")
			componentObj := v1alpha1.MicroservicesComponent{}
			err := json.Unmarshal(outputDetail, &componentObj)
			Expect(err).To(BeNil())
			status := componentObj.Status.Status
			if status == expectStatus {
				result = expectStatus
				return result
			}
			time.Sleep(time.Second * 1)
		}
	}
}

var _ = DescribeM("MicroserviceEnvironment", func() {
	var (
		microservicesComponent         *v1alpha1.MicroservicesComponent
		release                        *catav1.Release
		err                            error
		output                         []byte
		microservicesEnvironmentName   string
		displayName                    string
		description                    string
		file                           string
		microservicesComponentName     string
		asfData                        map[string]string
	)
	BeforeEach(func() {
		microservicesEnvironmentName = "e2easfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		displayName = "e2easfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		description = "e2easfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		microservicesComponentName = "zookeeper"

		
		file = "microservicesComponent/microservicesComponent.yaml"
		asfData = map[string]string{
			"${micoEnvironmentName}": microservicesEnvironmentName,
			"${displayName}": displayName,
			"${description}": description,
		}
		file = makeFile(file, asfData) 
		kubectlApply(file, true)

		// 创建zookeeper时依赖存储，目前环境里没有安装存储，所有PVC 会一直是Pending, 导致zookeeper 也是Pending
		waitMicroserviceEnvironmentChangeToStatus(microservicesEnvironmentName, microservicesComponentName, "Pending")
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
	})
	DescribeF("L1:AldDevops-2542:Create Zookeeper MicroservicesComponent success", func() {
		JustBeforeEach(func() {
			microservicesComponent, err = asfClient.AsfV1alpha1().MicroservicesComponents(microservicesEnvironmentName).Get(microservicesComponentName, v1.GetOptions{})
			time.Sleep(time.Second * 1)
		})
		It("catlog should have config-server, eureka, hystrix-dashboard, turbine, zuul", func() {
			output, err = commandP("kubectl", "get", "chart")

			Expect(err).To(BeNil())
			// catlog has config-server, eureka, hystrix-dashboard, turbine, zuul
			Expect(s.Contains(string(output), "config-server")).To(BeTrue());
			Expect(s.Contains(string(output), "eureka")).To(BeTrue());
			Expect(s.Contains(string(output), "hystrix-dashboard")).To(BeTrue());
			Expect(s.Contains(string(output), "turbine")).To(BeTrue());
			Expect(s.Contains(string(output), "zuul")).To(BeTrue());
			Expect(microservicesComponent.GetName()).To(Equal(microservicesComponentName))
			Expect(microservicesComponent.Status.Status).To(Equal("Pending"))
			
		})
	})


	DescribeF("L1:AldDevops-2543:it will auto create release after create Zookeeper MicroservicesComponent success", func() {
		JustBeforeEach(func() {
			release, err = catalogClient.CatalogControllerV1alpha1().Releases(microservicesEnvironmentName).Get(microservicesComponentName, v1.GetOptions{})
		})
		It("should contain a release "+microservicesComponentName, func() {
			Expect(err).To(BeNil())
			Expect(release.GetName()).To(Equal(microservicesComponentName))
		})
	})
})
