package v1alpha1_test

import (
	"fmt"
	// "strings"
	"time"
	
	"alauda.io/asf-apiserver/pkg/apis/asf/v1alpha1"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = DescribeM("MicroserviceEnvironment", func() {
	var (
		MicroservicesEnvironmentBinding          *v1alpha1.MicroservicesEnvironmentBinding
		// microservicesEnvironmentBindingList      *v1alpha1.MicroservicesEnvironmentBindingList
		// output                                   []byte
		err                                      error
		projectName                              string
		micoEnvironmentName                      string
		bindName                                 string
		bindProjectName                          string
		bindMicoEnvironmentName                  string
		displayName                              string
		description                              string
		file                                     string
		asfData                                  map[string]string
	)
	BeforeEach(func() {
		projectName = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		micoEnvironmentName = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		bindName = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		bindProjectName = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		bindMicoEnvironmentName = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		displayName = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())
		description = "e2etestasfenvironment" + fmt.Sprintf("%v", time.Now().UnixNano())

		// 创建project, micoservicesEnvironment
		file = "microservicesEnvironmentBinding/test-microservicesEnvironmentBinding.yaml"
		asfData = map[string]string{
			"${projectName}": projectName,
			"${micoEnvironmentName}": micoEnvironmentName,
			"${description}": description,
			"${displayName}": displayName,
			"${bindName}": bindName,
			"${bindProjectName}": projectName,
			"${bindMicoEnvironmentName}": micoEnvironmentName,
		}
		file = makeFile(file, asfData) 
		kubectlApply(file, true)
	})
	AfterEach(func() {
		commandP("kubectl", "delete", "-f", file)
	})
	DescribeF("L1:AldDevops-2537:Get micro services environment binding", func() {
		JustBeforeEach(func() {
		
			MicroservicesEnvironmentBinding, err = asfClient.AsfV1alpha1().MicroservicesEnvironmentBindings(projectName).Get(bindName, v1.GetOptions{})
			time.Sleep(time.Second * 1)
		})
		It("Should not error", func() {
			Expect(err).To(BeNil())
			Expect(MicroservicesEnvironmentBinding.GetName()).To(Equal(bindName))
		})
	})

	DescribeF("L1:AldDevops-2538:Delete micro services environment binding", func() {
		JustBeforeEach(func() {
			err = asfClient.AsfV1alpha1().MicroservicesEnvironmentBindings(projectName).Delete(bindName, &v1.DeleteOptions{})
		})
		It("should contain " + bindName, func() {
			Expect(err).To(BeNil())
		})
	})
})
