.PHONY: init build-base build unitest test
PROJ?=alauda.io/asf-apiserver
PWD=$(shell pwd)
APISERVERBUILDER=${GOPATH}/src/github.com/kubernetes-incubator/apiserver-builder
K8SCODE=${GOPATH}/src/k8s.io/kubernetes
TAG=dev-$(shell cat .version)-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudak8s/asf-apiserver
image=$(IMAGE)
tag=$(TAG)
PACKAGES = $(shell go list ./... | grep -v 'client' | grep -v 'mock' | grep -v vendor | grep -v 'test')
SET1=$(foreach pkg,$(shell ls ./pkg/ | grep -v client | grep -v mock), $(shell echo './pkg/'$(pkg)'/...'))
BASE_TOKEN=$(shell echo $(TAG))-int-dev
INT_TOKEN=$(shell echo $(BASE_TOKEN) | sed 's|\.|-|g')
PROTOC=$(shell which protoc)
NAMESPACE=alauda-system
NAME=asf-apiserver
SERVICE_ACCOUNT=asf-apiserver
IMAGE-PULL-SECRETS=alaudak8s

test:
	go test -cover -v $(PACKAGES)

init: init-builder init-gitversion init-k8scode
	
init-gitversion:
	go get -u github.com/alauda/gitversion

init-builder:
	rm -rf ${APISERVERBUILDER}
	go get -u github.com/kubernetes-incubator/apiserver-builder || true

init-k8scode:
	rm -rf ${K8SCODE}
	go get -u k8s.io/kubernetes || true
	cd ${K8SCODE} && git checkout v1.9.1

build:
	apiserver-boot build executables --goos linux --goarch amd64

k8s-init:
	kubectl apply -f artifacts/deploy/ns.yaml
	kubectl apply -f artifacts/deploy/sa.yaml
	kubectl apply -f artifacts/deploy/auth-delegator.yaml
	kubectl apply -f artifacts/deploy/auth-reader.yaml
	kubectl apply -f artifacts/deploy/secret.yaml
	kubectl apply -f artifacts/deploy/etcd-pod.yaml
	kubectl apply -f artifacts/deploy/deploy.yaml
	kubectl apply -f artifacts/deploy/service.yaml
	kubectl apply -f artifacts/deploy/apiservice.yaml

k8s-init-with-devops:
	kubectl apply -f artifacts/deploy/sa.yaml
	kubectl apply -f artifacts/deploy/auth-delegator.yaml
	kubectl apply -f artifacts/deploy/auth-reader.yaml
	kubectl apply -f artifacts/deploy/deploy.yaml
	kubectl apply -f artifacts/deploy/service.yaml
	kubectl apply -f artifacts/deploy/apiservice.yaml

k8s-drop-with-devops:
	kubectl delete -f artifacts/deploy/apiservice.yaml
	kubectl delete -f artifacts/deploy/sa.yaml
	kubectl delete -f artifacts/deploy/auth-delegator.yaml
	kubectl delete -f artifacts/deploy/auth-reader.yaml
	kubectl delete -f artifacts/deploy/deploy.yaml
	kubectl delete -f artifacts/deploy/service.yaml

k8s-drop:
	kubectl delete -f artifacts/deploy/apiservice.yaml
	kubectl delete -f artifacts/deploy/sa.yaml
	kubectl delete -f artifacts/deploy/auth-delegator.yaml
	kubectl delete -f artifacts/deploy/auth-reader.yaml
	kubectl delete -f artifacts/deploy/secret.yaml
	kubectl delete -f artifacts/deploy/etcd-pod.yaml
	kubectl delete -f artifacts/deploy/deploy.yaml
	kubectl delete -f artifacts/deploy/service.yaml
	kubectl delete -f artifacts/deploy/ns.yaml


build-image:
	rm -rf ./bin
	mkdir bin
	GO_ENABLED=0 GOOS=linux GOARCH=amd64 GOCACHE=off go build -ldflags "-w -s" -v -a -installsuffix cgo -o ./bin/apiserver cmd/apiserver/main.go
	GO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-w -s" -v -a -installsuffix cgo -o ./bin/controller-manager cmd/controller-manager/main.go
	upx ./bin/apiserver
	upx ./bin/controller-manager
	cp artifacts/images/Dockerfile ./bin/
	docker build -f ./bin/Dockerfile -t ${IMAGE}:${TAG} ./bin

push-image: build-image
	docker push ${IMAGE}:${TAG}

run: push-image
	$(MAKE) image=${IMAGE} tag=${TAG} update

update:
	kubectl set image deploy/asf-apiserver -n alauda-system apiserver=$(image):$(tag) controller=$(image):$(tag)
	kubectl scale deploy -n alauda-system asf-apiserver --replicas=0
	kubectl scale deploy -n alauda-system asf-apiserver --replicas=1

controller:
	go run main.go controller --kubeconfig ~/.kube/config --logtostderr -v 7

clean:
	rm -rf ./bin

.PHONY: gen-client
gen-client:
	./hack/update-codegen.sh

.PHONY: build-in-docker
build-in-docker:
	docker run --rm -v ${PWD}:/go/src/alauda.io/asf-apiserver -w /go/src/alauda.io/asf-apiserver index.alauda.cn/library/golang:alpine go install

.PHONY: cover
cover: collect-cover-data test-cover-html open-cover-html

collect-cover-data:
	echo "mode: count" > coverage-all.out
	@$(foreach pkg,$(PACKAGES),\
		go test -v -coverprofile=coverage.out -covermode=count $(pkg);\
		if [ -f coverage.out ]; then\
			tail -n +2 coverage.out >> coverage-all.out;\
		fi;)

test-cover-html:
	go tool cover -html=coverage-all.out -o coverage.html

test-cover-func:
	go tool cover -func=coverage-all.out

open-cover-html:
	open coverage.html

check: check-test check-coverage

check-full: check-lint check-test check-coverage

check-lint:
	gometalinter.v2 --checkstyle --exclude='(.)*/zz_(.)*go' --exclude='(.)*_test(.)go' --exclude='(.)*/generated(.)*go' --deadline 10m $(SET1) > report.xml || true

check-coverage:
	gocov test $(PACKAGES) | gocov-xml > coverage.xml

check-test:
	go test -v $(PACKAGES) | go-junit-report > test.xml

set:
	echo $(PROTOC)

build-test-image:
	docker build -t index.alauda.cn/alaudak8s/asf-apiserver-tests:$(INT_TOKEN) -f artifacts/images/Dockerfile.test .
	docker push index.alauda.cn/alaudak8s/asf-apiserver-tests:$(INT_TOKEN)

integration: build-test-image int-cleanup
	cp test/integration-tests.yaml tmp.yaml
	sed -i '' "s|%TOKEN%|$(INT_TOKEN)|g" tmp.yaml
	sed -i '' "s|%IMAGE%|index.alauda.cn/alaudak8s/asf-apiserver-tests:$(INT_TOKEN)|g" tmp.yaml
	kubectl create -f tmp.yaml

int-cleanup:
	kubectl delete -f tmp.yaml || true
	curl -X DELETE -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/
	rm tmp.yaml || true

int-status:
	curl -H 'Authorization: Token '$(INT_TOKEN) http://sonobuoy-alaudak8s.myalauda.cn/status/ | python -m json.tool

gen-protobuf:
	./hack/generate-proto.sh

gen-mock:
	mockgen -destination ./pkg/mock/client-go/informers/factory.go -package informers k8s.io/client-go/informers SharedInformerFactory
	mockgen -destination ./pkg/mock/asf/informers/factory.go -package informers alauda.io/asf-apiserver/pkg/client/informers/externalversions SharedInformerFactory
	mockgen -destination ./pkg/mock/asf/controller/subcontroller.go -package informers alauda.io/asf-apiserver/pkg/controller SubController

deployment:
	apiserver-boot run in-cluster --name ${NAME} --namespace ${NAMESPACE} --image ${IMAGE}:${TAG}

# Regenerates api-machinery code
build-generated:
	apiserver-boot build generated

# Builds the deployment config for the aggregated api server and an etcd store to back it
.PHONY: build-agg-config
build-agg-config:
	rm -rf config
	apiserver-boot build config  --name ${NAME} --namespace ${NAMESPACE} --image ${IMAGE}:${TAG} --service-account ${SERVICE_ACCOUNT} --image-pull-secrets ${IMAGE-PULL-SECRETS}

# Runs the aggregated api and clears the discovery cache before outputing registered apis
.PHONY: run-agg-api
run-agg-api: build-agg-config 
	kubectl apply -f config/apiserver.yaml
	rm -rf ~/.kube/cache/discovery/
	kubectl api-versions 

run-local:
	rm -rf config
	apiserver-boot build config --local-minikube --name ${NAME} --namespace ${NAMESPACE}
	kubectl create -f config/apiserver.yaml
	apiserver-boot run local-minikube