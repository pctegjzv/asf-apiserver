#!/usr/bin/env bash
cd /go/src/alauda.io/asf-apiserver && GO_ENABLED=0 GOOS=linux GOARCH=amd64 go build  -v -x -o ./bin/apiserver cmd/apiserver/main.go